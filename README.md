# Arch Linux Installation Guide for Beginners
Last Updated: 8-July-2023

Before you start the Arch Linux installation you should make sure that all the necessary requirements are met. Your computer should have at least **512 MB of RAM** and **1 GB of memory**. For streamlined use, **2 GB of RAM** and **10 GB of storage are recommended**. The computer should also be **x86-64 compatible**. A working **Internet connection** and a **USB drive** with a storage capacity of at least 2 GB are also important.

The method discussed here **wipes out existing operating system(s)** from your computer and installs Arch Linux on it. So if you are going to follow this tutorial, make sure that you have backed up your files, or else you’ll lose all of them. **You have been warned**.

## Pre-installation

First, download the Arch Linux installation ISO from the [Arch Linux website](https://archlinux.org/download/).

You can download the torrent file or use the direct link. Just find your country and click on the link. Among the variety of files, choose the ISO archlinux-xxxx.xx.xx-x86_64.iso and signature archlinux-xxxx.xx.xx-x86_64.iso.sig files:

### Create a Live USB

When the ISO is downloaded, you need to check its signature to make sure it has not been compromised:

```
gpg --keyserver-options auto-key-retrieve --verify /path/to/archlinux.iso.sig

```

If you see “Good signature from …“, this means everything is alright: 
```
➜  Downloads gpg --keyserver-options auto-key-retrieve --verify archlinux-2023.01.01-x86_64.iso.sig  
gpg: assuming signed data in 'archlinux-2023.01.01-x86_64.iso'
gpg: Signature made Sun 01 Jan 2023 09:49:49 PM +06
gpg:                using EDDSA key 3E80CA1A8B89F69CBA57D98A76A5EF9054449A5C
gpg: key 76A5EF9054449A5C: public key "Pierre Schmitz <pierre@archlinux.org>" imported
gpg: Total number processed: 1
gpg:               imported: 1
gpg: Good signature from "Pierre Schmitz <pierre@archlinux.org>" [unknown]
gpg:                 aka "Pierre Schmitz <pierre@archlinux.de>" [unknown]
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 3E80 CA1A 8B89 F69C BA57  D98A 76A5 EF90 5444 9A5C

```

Next, you need to write it to your USB flash drive. Open the Linux terminal and use the following command:
```
dd bs=4M if=/path/to/archlinux.iso of=/dev/sdx status=progress && sync
```
where `/path/to/archlinux.iso` is the path to your downloaded ISO file; `/dev/sdx` is your flash drive. You can find out this name with the command `sudo fdisk -l` and the size of your USB flash drive.

### Verify BIOS or UEFI Mode

If UEFI mode is enabled on an UEFI motherboard, Archiso will boot Arch Linux accordingly via systemd-boot. To verify this, list the efivars directory:

```
# ls /sys/firmware/efi/efivars
```
OR 
```
# efivar -l
```

If the command shows the directory without error, then the system is booted in UEFI mode. If the directory does not exist, the system may be booted in [BIOS](https://en.wikipedia.org/wiki/BIOS) (or [CSM](https://en.wikipedia.org/wiki/Compatibility_Support_Module)) mode. If the system did not boot in the mode you desired, refer to your motherboard's manual.

### Connecting to the Internet
To check if your internet works, you need to ping to any server, for example, the Arch Linux website:

```
# ping -c 5 archlinux.org
```

If ping doesn't work, you'll have to manually configure it. Enter your interface with the `dhcpcd` command and hope to get connected. And if you're using wired broadband connection then try with [pppd](https://wiki.archlinux.org/index.php/Ppp).

> **Note:** In the installation image, [systemd-networkd](https://wiki.archlinux.org/index.php/Systemd-networkd), [systemd-resolved](https://wiki.archlinux.org/index.php/Systemd-resolved) and [iwd](https://wiki.archlinux.org/index.php/Iwd) are preconfigured and enabled by default. That will not be the case for the installed system.

If you are not sure what interfaces are available, use
``` 
# ip link
```

For Wi-Fi, launch `iwctl` prompt:

```
# iwctl
```

List available devices:
```
# device list
```

You should see your Wi-Fi available with a name like `wlan0`.

Scan for networks and list available Wi-Fi networks:

```
# station wlan0 scan #this command outputs nothing
# station wlan0 get-networks
```

Finally, to connect to your Wi-Fi network:
```
# station wlan0 connect my_wifi_name
```

You will be requested to enter the passphrase of your Wi-Fi and your Wi-Fi connection will be established.

Exit the iwctl and test the connection again:
```
# exit
# ping -c 5 archlinux.org
```

**Mobile broadband modems** can be configured with the `nmcli utility`.

### Update the system clock

To ensure the system clock is accurate:
```
# timedatectl set-ntp true
```
To check the service status, use `timedatectl status`

### Partitioning Disk

When recognized by the live system, disks are assigned to a [block device](https://wiki.archlinux.org/index.php/Block_device) such as `/dev/sda`, `/dev/nvme0n1` or `/dev/mmcblk0`. To identify these devices, use [`lsblk`](https://wiki.archlinux.org/index.php/Lsblk) or *fdisk*.

> ### Note for beginner
>
> First, let me say one thing, please don't get in trouble with the partitioning. You can ruin your entire hard disk. So I would say, before you make a separate partition, You can do this with windows or any of your other Linux distro. For Arch you need at least 10 GB of partition, and you can add swap if you want.

We will now create the partitions on the hard disk for the OS installation. First, list the available disks using the fdisk command.

    # fdisk -l /dev/sdX

> Note: If the device is not specified, *fdisk* will list all partitions in `/proc/partitions`.

The following [partitions](https://wiki.archlinux.org/index.php/Partition) are **required** for a chosen device:

*   One partition for the [root directory](https://en.wikipedia.org/wiki/Root_directory) `/`.
*   For booting in [UEFI](https://wiki.archlinux.org/index.php/UEFI) mode: an [EFI system partition](https://wiki.archlinux.org/index.php/EFI_system_partition).

#### Creating Partition Table

Start `fdisk` against your drive as root.

    # fdisk /dev/sdX

> **Warning:** Creating a new partition table will erase entire disk.

To create a new partition table and clear all current partition data type **o** at the prompt for a MBR partition table or **g** for a GUID Partition Table (GPT). Skip this step if the table you require has already been created.

#### Creating Partitions

Create a new partition with the **n** command.

When prompted, specify the partition type, type **p** to create a primary partition or **e** to create an extended one. There may be up to four primary partitions.

The first sector must be specified in absolute terms using sector numbers. The last sector can be specified using the absolute position in sectors or using the **+** symbol to specify a position relative to the start sector measured in sectors, kibibytes (**K**), mebibytes (**M**), gibibytes (**G**), tebibytes (**T**), or pebibytes (**P**); for instance, setting **+2G** as the last sector will specify a point 2GiB after the start sector. Pressing the **Enter** key with no input specifies the default value, which is the start of the largest available block for the start sector and the end of the same block for the end sector.

> **Note for UEFI:** <br> The *EFI system partition (ESP)*, a small partition formatted with FAT32, is usually around 500MB, this is where the EFI boot loaders and applications used by the firmware at system during start-up are stored. If your hard drive is in the *GUID Partition table (GPT)* partition style, it will automatically generate an EFI system partition after you have installed your operating systems. Both Windows and Mac operating systems are supported.
>
> *   Minimum Partition Size *At least 260 MiB*
> *   [EFI system partition](https://wiki.archlinux.org/index.php/EFI_system_partition) requires parition type `EFI System`.
> *   If the disk from which you want to boot [already has an EFI system partition](https://wiki.archlinux.org/index.php/EFI_system_partition#Check_for_an_existing_partition), do not create another one, but use the existing partition instead.
> *   Mount Point `/mnt/boot` or `/mnt/efi` or `/mnt/boot/efi`

> **Note for BIOS**:<br> BIOS is not required *efi* partition. You may proceed without creating *efi* partition.

> **Note for Swap Partition:**<br> It is recommended to use Linux swap for any swap partitions, since systemd will automount it.

#### `fdisk` Options

Partition Table: </br>
* **g** for GPT partition table 
* **o** for MBR/DOS partition table 

Basic: </br>
* **d** delete a partition
* **n** create a new parition 
* **p** print partition information 
* **t** change partition type 

Save and Exit: </br>
* **w** write 
* **q** for quit

EFI Partition

![efi-partition](images/efi.png)

#### Screenshot 
Root Partition

![root](images/root.png)

Swap Partition 

![swap](images/swap.png)

> Write the table to disk and exit via the **w** command.

#### Format File System

Once the partitions have been created, each must be formatted with an appropriate [file system](https://wiki.archlinux.org/index.php/File_system). For example, if the root partition is on `/dev/sdX0` and will contain the **Ext4** file system.

> Check your partition table using `lsblk`.

    # mkfs.ext4 /dev/sdX0

The UEFI specification mandates support for the FAT12, FAT16, and FAT32 file systems. To prevent potential issues with other operating systems and also since the UEFI specification only mandates supporting FAT16 and FAT12 on removable media, it is recommended to use FAT32.

After creating the partition, [format](https://wiki.archlinux.org/index.php/Format) it as [FAT32](https://wiki.archlinux.org/index.php/FAT32). To use the `mkfs.fat` utility, [install](https://wiki.archlinux.org/index.php/Install) [dosfstools](https://www.archlinux.org/packages/?name=dosfstools).

    # mkfs.fat -F32 /dev/sdX0

If you created a partition for **swap**, initialize it with *mkswap*:

    # mkswap /dev/sdX0
    # swapon /dev/sdX0

[genfstab](https://man.archlinux.org/man/genfstab.8) will later detect mounted file systems and swap space.

#### Mount File System

[Mount](https://wiki.archlinux.org/index.php/Mount) the root volume to `/mnt`. For example, if the root volume is `/dev/sdX`:

    # mount /dev/sdX0 /mnt

Mount Point for EFI partition`/mnt/efi` or `/mnt/boot/efi` for mounting EFI system partition are:

    # mount /dev/sdX0 /mnt/boot/efi

> If `/mnt/efi` file doesn't exist then create it manually. Using `mkdir -p /mnt/boot/efi`

## Installation

### Select the mirrors

To view world wide server list, please visit [Arch Liux Server List](https://www.archlinux.org/mirrorlist/all/). I'm using **xeonbd** server as Bangladesh mirror server!

    # vim /etc/pacman.d/mirrorlist

Add `Server = http://mirror.xeonbd.com/archlinux/$repo/os/$arch` top of the file. Then save and exit. The higher a mirror is placed in the list, the more priority it is given when downloading a package.

**Sorting mirrors using [reflector](https://wiki.archlinux.org/title/Reflector)**

1. Install `reflector` package 
2. `/etc/pacman.d/mirrorlist` will be overwritten. Make a backup before proceeding.
3. To see all of the available options, run the following command: `reflector --help`

### Install essential packages

    # pacstrap -K /mnt base base-devel linux linux-firmware linux-headers vim

## Configure the system

### fstab

Generate an [fstab](https://wiki.archlinux.org/index.php/Fstab) file:

    # genfstab -U /mnt >> /mnt/etc/fstab

Check the resulting `/mnt/etc/fstab` file, and edit it in case of errors. To learn what -U and -p mean, type genfstab -help to see the description of these options.

### Chroot

Next, chroot (change root) to your system that is mounted to /mnt using the BASH environment:

[Change root](https://wiki.archlinux.org/index.php/Change_root) into the new system:

    # arch-chroot /mnt

### Time Zone

    # ln -sf /usr/share/zoneinfo/Asia/Dhaka /etc/localtime

Run `hwclock` to generate `/etc/adjtime`:

    # hwclock --systohc --utc

And check the time:
```bash 
date 
```
If the time is incorrect, go back and make sure you have set the timezone correctly.

### Localization

Uncomment `en_US.UTF-8 UTF-8` and `en_US ISO-8859-1` from selected file, and then save it.

    # vim /etc/locale.gen

Generate locale file:

    # locale-gen

Create the `locale.conf` file, and set the `LANG` variable accordingly:

    # vim /etc/locale.conf

```
LANG=en_US.UTF-8
```

Shortcut Command : `echo "LANG=en_US.UTF-8" > vim /etc/locale.conf`

### Network Configuration

Just enter your host name. For example `admin@example ->`, here "admin" is username and "example" is the host name. 
```
# vim /etc/hostname
```

Shortcut Command : `echo "myhostname" > /etc/hostname`

Add matching entries to `hosts`:

```
# vim /etc/hosts
```

    127.0.0.1   localhost
    ::1         localhost
    127.0.1.1   myhostname.localdomain	myhostname

If the system has a permanent IP address, it should be used instead of `127.0.1.1`.

### Root Password

Set the root [password](https://wiki.archlinux.org/index.php/Password):

    # passwd

### Boot Loader (Grub)

A boot loader is a piece of software started by the firmware ([BIOS](https://en.wikipedia.org/wiki/BIOS) or [UEFI](https://wiki.archlinux.org/index.php/UEFI)). It is responsible for loading the kernel with the wanted [kernel parameters](https://wiki.archlinux.org/index.php/Kernel_parameters), and [initial RAM disk](https://wiki.archlinux.org/index.php/Mkinitcpio) based on configuration files. In the case of UEFI, the kernel itself can be directly launched by the UEFI using the EFI boot stub. A separate boot loader or boot manager can still be used for the purpose of editing kernel parameters before booting.

> If you have an Intel or AMD CPU, enable [microcode](https://wiki.archlinux.org/index.php/Microcode) updates in addition.

*   BIOS (Legacy Mode)

        # pacman -S grub
        # grub-install --target=i386-pc /dev/sdX
        # grub-mkconfig -o /boot/grub/grub.cfg

*   UEFI

    > The section assumes you are installing GRUB for x86\_64 systems. For IA32 (32-bit) UEFI systems (not to be confused with 32-bit CPUs), replace `x86_64-efi` with `i386-efi` where appropriate.

        # pacman -S grub efibootmgr
        # grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
        # grub-mkconfig -o /boot/grub/grub.cfg

### Network Manager

    # pacman -S networkmanager

Automate Network Configuration

    # systemctl enable NetworkManager.service

### Completing Installation

Exit the chroot environment by typing `exit` or pressing `Ctrl+d`. Optionally manually unmount all the partitions with `umount -R /mnt`: this allows noticing any "busy" partitions, and finding the cause with **fuser**.

    # exit
    # umount -R /mnt
    # reboot

Finally, restart the machine by typing `reboot`: any partitions still mounted will be automatically unmounted by systemd. Remember to remove the installation medium and then login into the new system with the root account.

> ### Note
> while login after reboot use **root** account and it's password!

# Post-installation (To Do after install)

## Create User

It is not a good idea to constantly work from the root account. So, after the login, create a user account:

Enter your user name on `username`. For example `admin@example ->`, `admin` is user name and `example` is the host name.

    # useradd -m -G wheel -s /bin/sh username

Adding password for the user:

    # passwd username

Enable sudo privileges for a newly created user:
Find the line from `/etc/sudoers` and uncomment it, by removing the **#** sign.

```bash
# %wheel ALL=(ALL) ALL
```

## Graphical user interface

How does desktop environment works [Base Components of the Linux GUI](https://turlucode.com/arch-linux-install-guide-step-2-desktop-environment-installation/)

### Display Server

[Xorg](https://wiki.archlinux.org/title/Xorg) is the public, open-source implementation of the X Window System (commonly X11, or X). It is required for running applications with graphical user interfaces (GUIs), and the majority of users will want to install it.

[Wayland](https://wiki.archlinux.org/title/Wayland) is a newer, alternative display server protocol with several compositors to choose from.


* [Xorg](https://wiki.archlinux.org/title/Xorg) <br>
	```
	# pacman -S xorg-server xorg-xinit
	```

* [Wayland](https://wiki.archlinux.org/title/Wayland) <br>
	Ideally, you should have the base wayland package installed already. Open a terminal and verify running the below command. **Remember only `GDM` and `SDDM` Supports Wayland.**
	```
	# pacman -Qi wayland
	```
	If it is not installed, install it using:
	```
	# pacman -S --needed wayland
	```

### Graphics Driver Installation

The default *modesetting* display driver will work with most video cards, but performance may be improved and additional features harnessed by installing the appropriate driver for AMD or NVIDIA products. Check your graphic card:

    # lspci -k | grep -A 2 -i "VGA"

* **[Nvidia GPU Driver](https://wiki.archlinux.org/title/NVIDIA)** <br>

>**Warning**: Avoid installing the NVIDIA driver through the package provided from the NVIDIA website. Installation through pacman allows upgrading the driver together with the rest of the system.

```
# pacman -S nvidia nvidia-utils nvidia-settings xorg-server-devel opencl-nvidia
```

Check your driver, it's working or not:
```
# nvidia-smi
```
	
* **[Intel Graphics](https://wiki.archlinux.org/title/intel_graphics)** <br>
Since Intel provides and supports open source drivers, Intel graphics are essentially plug-and-play.
```
# pacman -S xf86-video-intel mesa
```

* **[AMD GPU Driver](https://wiki.archlinux.org/title/AMDGPU)** <br>

Basically AMD drivers are pre-installed with kernel. You'll need to install a few packages to get the most out your GPU:

    # pacman -S xf86-video-amdgpu vulkan-radeon libva-mesa-driver mesa-vdpau

* `xf86-video-amdgpu`: The open-source driver package for newer AMD cards. You may also wish to install xf86-video-vesa as a fallback driver in case something breaks.
* `vulkan-radeon`: Vulkan support.
* `libva-mesa-driver` and `mesa-vdpau`: Hardware video encode and decode

There are some 32-bit packages you can also install, if you believe you might need them.

* **[Virual Box Guest Addition](https://wiki.archlinux.org/title/VirtualBox/Install_Arch_Linux_as_a_guest)** <br>
If you install the system on VirtualBox, also install guest addition:
``` 
# pacman -S virtualbox-guest-utils
```


### Desktop Environment
Select your favorite desktop [Desktop environment.](https://wiki.archlinux.org/index.php/Desktop_environment)

#### XFCE

    # pacman -S xfce4 xfce4-goodies 
	# echo "exec startxfce4" > ~/.xinitrc

run your desktop using `startxfce4`

#### GNOME

For my gnome-setup, please take a look [my gnome desktop](https://gitlab.com/farhansadik/my_arch-setup/-/wikis/My-Gnome-Desktop-Setup)

    # pacman -S gnome gnome-extra
	# echo "exec gnome-session" > ~/.xinitrc

#### KDE

    # pacman -S plasma kde-applications

#### Cinnamon

    # pacman -S cinnamon
	# echo "exec cinnamon-session" > ~/.xinitrc

#### [Pantheon](https://gitlab.com/farhansadik/pantheon-installer)

Before try please visit for latest notice <https://gitlab.com/farhansadik/pantheon-installer>.

```bash
$ git clone https://gitlab.com/farhansadik/pantheon-installer
$ cd pantheon-insaller 
```

You must run `installer` file wihout `root` .

```bash
$ ./install
```

#### Mate

	# sudo pacman -S mate 
	# echo "exec mate-session" > ~/.xinitrc

#### Deepin

	# pacman -S deepin
	# echo "exec startdde"  > ~/.xinitrc

#### UKUI

	# pacman -S ukui
	
#### Cutefish

	# pacman -S cutefish

### Display Manager

1.  **LightDM**

        # pacman -S lightdm lightdm-gtk-greeter

    Start and Enable Lightdm

        # systemctl enable lightdm.service 
        # systemctl start lightdm.service

	Lightdm Theme ~ [litarvan](https://github.com/Litarvan/lightdm-webkit-theme-litarvan)

2.  **GDM**

        # pacman -S gdm
        # systemctl enable gdm.service

3.  **SDDM** 

	    # pacman -S sddm # systemctl enable sddm.service


## Important Packages

You'll need to install those packages for better support:

*   gvfs
*   ntfs-3g
*   os-prober


## Source

1.  [Official Arch Installation Guide](https://wiki.archlinux.org/index.php/Installation_guide)
2.  [EFI system partition - ArchWiki (archlinux.org)](https://wiki.archlinux.org/index.php/EFI_system_partition#Format_the_partition)
3.  [GRUB - ArchWiki (archlinux.org)](https://wiki.archlinux.org/index.php/GRUB#GUID_Partition_Table_\(GPT\)_specific_instructions)
4.  [UEFI - Community Help Wiki (ubuntu.com)](https://help.ubuntu.com/community/UEFI)
5.  [Linux LTS Kernel Installation](https://gitlab.com/farhansadik/my_arch-setup/-/wikis/Linux-LTS-Kernel)
6. [General Recommendations](https://wiki.archlinux.org/title/General_recommendations)
